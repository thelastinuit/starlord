FROM ruby:2.7-rc-alpine3.10

WORKDIR /code

RUN mkdir -p /code  /config

RUN \
    apk update && \
    apk --no-cache --update add \
    git postgresql-dev tzdata make g++ wget curl inotify-tools \
    nodejs nodejs-npm && \
    npm install npm -g --no-progress && \
    update-ca-certificates --fresh && \
    rm -rf /var/cache/apk/*

ENV PATH=./node_modules/.bin:$PATH \
    HOME=/code

RUN gem install bundler
RUN npm install -g yarn

ADD Guardfile /config
ADD .reek /config
ADD .rubocop.yml /config

RUN gem install rubocop reek guard guard-rubocop guard-reek

CMD ["guard", "--guardfile", "/config/Guardfile", "--no-bundler-warning", "--group", "drax_is_watching"]
